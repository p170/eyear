#!/usr/bin/env python3
import threading
from threading import Thread
import multiprocessing
import socket
import string
import random
import filecmp
import os
import sys


from datetime import datetime
#import btserver.server as bts
from btserver.server import BtServer
from listener.listener import Recorder
import btserver.commontools as c
from others.ledhandler import LedThread
from tf.tfprocessing import TensorFlowProcessing
import atexit
import logging
import sys
import pydevd_pycharm
#pydevd_pycharm.settrace('192.168.10.146', port=12345, stdoutToServer=True, stderrToServer=True)
#pydevd_pycharm.settrace('192.168.0.104', port=12345, stdoutToServer=True, stderrToServer=True)
#pydevd_pycharm.settrace('192.168.0.101', port=12345, stdoutToServer=True, stderrToServer=True)


from termcolor import colored

class CommandSwitch:
    # 1 49 Red led  48 - Not listening  49 - Listening  50 - Recording for 5 second
    # 2 50 Blue led conintous light - DeviceConnectedStates   20 - Idle 21 - Waiting for Connection 22 - Connected  23- Disconnected
    # 3 51 Request sound data
    # 4 52 Sound Data
    # 5 53 Stop request sound data
    # 6 54 Rquest TEnsor
    # 7 55 Stop Tensorflow
    # 8 56 SetMicrophoneParameters
    # 9 57 DeviceIsReady

    def __init__(self):
        self.isConnected = False
        self.isRecording = True
        self.evaluation = False
        self.isDeviceReady = False

    def lookupMethod(self, command):
        return getattr(self, '_' + command, None)

    # 1 49 Red led  48 - Not listening  49 - Listening  50 - Recording for 5 second
    def _49(self,data,threadedServer):
        #print (colored("Record ág.", "yellow"))

        if data[1] ==48:
            try:
                rcp_logger.info("Turn off red led.")
                #print (colored("Turn off red led.", "yellow"))
                threadedServer.ledthread.set_recordingState(0)
                self.isRecording = False
                self.evaluation = False


            except Exception as e:
                print(e)
        if data[1] == 49:
            try:
                rcp_logger.info("Blink red led.")
                #print (colored("Blink red led.", "yellow"))
                threadedServer.ledthread.set_recordingState(1)
                self.isRecording = True
                self.evaluation = False

            except Exception as e:
                print(e)

        if data[1] == 50:
            try:
                rcp_logger.info("Hold red led.")
                #print (colored("Turn on red led.", "yellow"))
                threadedServer.ledthread.set_recordingState(2)
                self.isRecording = True
                self.evaluation = True

            except Exception as e:
                print(e)

        threadedServer.btServer.c.isRecording = self.isRecording
        threadedServer.btServer.c.isEvaulating = self.evaluation

    # 2 50 Blue led conintous light - DeviceConnectedStates   20 - Idle 21 - Waiting for Connection 22 - Connected  23- Disconnected
    def _50(self,data,threadedServer):
        try:
            if data[1] ==48:
                rcp_logger.info("Idle Bt State.")
                print (colored("Idle Bt State.", "yellow"))
                self.isConnected = False
                threadedServer.ledthread.set_connectionState(0)

            if data[1] == 49:
                rcp_logger.info("WaitingForConnection.")
                self.isConnected = False
                #print (colored("WaitingForConnection.", "yellow"))
                threadedServer.ledthread.set_connectionState(1)

            if data[1] == 50:
                rcp_logger.info("Connected.")
                print (colored("Connected.", "yellow"))
                self.isConnected = True
                self.sendRequestTf = False
                self.sendRecordedData = False
                threadedServer.ledthread.set_connectionState(2)

            if data[1] == 51:
                rcp_logger.info("Disconnected. Reinitizal.")
                print (colored("Disconnected. Reinitizal.", "yellow"))
                self.isConnected = False
                self.sendRecordedData = False

                threadedServer.ledthread.set_connectionState(0)
                threadedServer.btServer = BtServer()
                threadedServer.btServer.start()
                threadedServer.tensorflowThread.btserver = threadedServer.btServer

            threadedServer.btServer.c.isConnected = self.isConnected
            threadedServer.btServer.c.isRecording = self.isRecording
            threadedServer.btServer.c.isEvaulating = self.evaluation
        except Exception as e:
            print(e)

    # 3 51 Request sound data
    def _51(self,data,threadedServer):
        rcp_logger.info("RequestSoundData")
        print (colored("RequestSoundData", "yellow"))
        self.sendRecordedData =True

    # 4 52 Sound Data
    def _52(self,data,threadedServer):
        try:
            fileName = data[1:].decode('utf8')
            if self.isConnected and self.sendRecordedData:
                threadedServer.btServer.packet.makePacketsFromFile(fileName)

            if self.isConnected and self.sendRequestTf:
                threadedServer.tensorflowThread.setMfccToProcess(fileName)
                print (colored("Tensorflow result TCP.", "yellow"))
        except Exception as e:
            print(e)

    # 5 53 Stop request sound data
    def _53(self,data,threadedServer):
        rcp_logger.info("StopRequestSoundData")
        print (colored("StopRequestSoundData", "yellow"))
        self.sendRecordedData = False

    # 6 54 Rquest TEnsor
    def _54(self,data,threadedServer):
        print(colored("RequestTensorflow.", "yellow"))
        self.sendRequestTf = True

    # 7 55 Stop Tensorflow
    def _55(self,data,threadedServer):
        self.sendRequestTf = False
        print (colored("Stop tensorflow results send.", "yellow"))

    # 8 56 SetMicrophoneParameters
    def _56(self,data,threadedServer):
        threadedServer.listener.setNewMicrophoneParameters(threadedServer.btServer.ginopDevice.currentMicSettings)
        print (colored("Microphone set", "yellow"))

    # 9 57 DeviceIsReady
    def _57(self, data, threadedServer):
        print(colored("Device IS Ready", "yellow"))
        self.isDeviceReady = True
        threadedServer.btServer.c.isDeviceReady = self.isDeviceReady
        if(self.isConnected):
            threadedServer.btServer.ginopDevice.sendDeviceIsReady()




class ThreadedServer(threading.Thread):
    def __init__(self):
        try:
            super(ThreadedServer, self).__init__()
            self.host = ''
            self.port = 12345

            self.btServer = BtServer()
            self.btServer.c.isDeviceReady = False
            self.ledthread = LedThread()
            self.tensorflowThread = TensorFlowProcessing(self.btServer)
            self.listener = Recorder()
            self.cmds = CommandSwitch()

            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.sock.bind((self.host, self.port))
            self.ledthread.start()
            self.btServer.start()
            self.listener.start()
            self.tensorflowThread.start()
        except Exception as e:
            print(e)

    def run(self):
        print (colored("Kommunikációs szerver elindult.", "green"))
        self.sock.listen(5)
        while True:
            self.client, address = self.sock.accept()
            self.client.settimeout(60)
            threading.Thread(target = self.listenToClient,args = (self.client,address)).start()

    def listenToClient(self, client, address):
        size = 1024

        while True:
            try:
                data = client.recv(size)
                if data:
                    #print (colored("Bejövő adat a tcp-re " , "red"))
                    # Set the response to echo back the recieved data


                    response = data
                    #print(list(data))

                    self.cmds.lookupMethod(str(data[0]))(data,self)
                    client.send(response)

            except Exception as e:
                #print("Vmi exception")
                rcp_logger.info(e)
                #print(e)
                client.close()
                return False




    def killAllthreads(self):
        print(colored("Leáll.", "magenta"))
        self.client.close()
        self.sock.shutdown(socket.SHUT_RDWR)
        self.sock.close()
        self.btServer.clearSockets()
        self.ledthread.setLedParametersToDefaultAfterProgramShutdowned()





##--------------Main-------------
log_file = '/media/external/logs/rcp/' + str(datetime.utcnow().strftime('%m_%d_%Y_%I_%M_%S')) + '.log'
# second file logger
rcp_logger = c.setup_logger('rcp_logger', log_file)
rcp_logger.info("RCP inicializálás")
print(colored("RCP inicializálás", "magenta"))


t = ThreadedServer()
t.start()









