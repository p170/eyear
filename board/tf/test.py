#!/usr/bin/python3
import tensorflow.keras
import h5py
import os
import time
import sys
import tensorflow as tf
import numpy as np
import tensorflow.keras.backend as K
from tensorflow.keras.models import model_from_yaml



sess = tf.Session()
K.set_session(sess)

print ("Open model")
yaml_file = open("model1.yaml", 'r')
loaded_model_yaml = yaml_file.read()
yaml_file.close()
print("create the model")
loaded_model = model_from_yaml(loaded_model_yaml)
    
# load weights into new model
print("load the weights")
loaded_model.load_weights("model1.h5")
print("Loaded model from disk")

print("Evaluate modell")
# evaluate loaded model on test data
loaded_model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
print("Model evaluated!")

loaded_model.summary()

print ("Loading saved pre-processed data...")
#data = np.load("2503_mfcc_feat_4096.npz")
#measurement_x = data['x']
#real_y = data['y']
print("Data loaded!")
# Predict the value
start_time = time.time()

# Load the training data into two NumPy arrays, for example using `np.load()`.
with np.load("2503_mfcc_feat_4096.npy") as data:
  features = data["features"]
  labels = data["labels"]

# Assume that each row of `features` corresponds to the same row as `labels`.
assert features.shape[0] == labels.shape[0]

dataset = tf.data.Dataset.from_tensor_slices((features, labels))


print("Time :" , "{0:.2f}".format((time.time() - start_time)/len(real_y)))

