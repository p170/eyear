#!/usr/bin/python


import time
import threading

import h5py
import os
import time
import sys
import tensorflow as tf
import numpy as np
import tensorflow.keras.backend as K
from tensorflow.keras.models import model_from_yaml
import commontools as c


class TensorFlowProcessing (threading.Thread):

    def __init__(self,btserver):
        self.lock = threading.RLock()
        self.btserver = btserver
        self.hasNewMfcc = False
        self.mfcc = None


        super(TensorFlowProcessing, self).__init__()



    def run(self):
        self.sess = tf.Session()
        K.set_session(self.sess)

        print ("Open model")
        self.yaml_file = open("/root/ginop/tf/model1.yaml", 'r')
        loaded_model_yaml = self.yaml_file.read()
        self.yaml_file.close()
        print("create the model")
        self.loaded_model = model_from_yaml(loaded_model_yaml)

        # load weights into new model
        print("load the weights")
        self.loaded_model.load_weights("/root/ginop/tf/model1.h5")
        print("Loaded model from disk")

        print("Evaluate modell")
        # evaluate loaded model on test data
        self.loaded_model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
        print("Model evaluated!")
        c.sendMessageViaClient("9")
        #self.loaded_model.summary()
        while True:
            if self.hasNewMfcc:
                self.hasNewMfcc = False
                ''' Load the training data into two NumPy arrays, for example using `np.load()`.
                with np.load("2503_mfcc_feat_4096.npy") as data:
                    features = data["features"]
                    labels = data["labels"]

                # Assume that each row of `features` corresponds to the same row as `labels`.
                assert features.shape[0] == labels.shape[0]

                dataset = tf.data.Dataset.from_tensor_slices((features, labels))'''
                tData = [1,1]
                temptens =self.btserver.packet.createNewPacket(0x07,tData,False)
                self.btserver.s.appendToQueueTf(temptens)


            time.sleep(0.1)  # let it breathe



    def setMfccToProcess(self,fileName):
        with self.lock:
            self.hasNewMfcc = True
            self.mfcc = fileName


















