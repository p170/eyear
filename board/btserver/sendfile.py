#!/usr/bin/python3

import threading
import time
import bluetooth
from enum import Enum
from termcolor import colored
import array
import socket
import multiprocessing

# some_file.py
import sys


from datetime import datetime
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '/root/ginop/rcp/btserver/')
from packet import Packet
from ginop import GinopDevice
import commontools as c
import logging
try:
    import queue
except ImportError:
    import Queue as queue


class SendFile(threading.Thread):
    def __init__(self,client_sock,btserver):
        print (colored("Send File Osztály Létrehozva", "magenta"))
        self.q = queue.Queue()
        self.client_sock =client_sock
        self.lock = threading.RLock()
        self.waitForSendPacketAck = False
        self.waitForSpecifySeq =0
        self.canContinue = False
        self.makeFakeAck = False
        self.btserver = btserver
        self.canStopSending = False
        self.hasErrorBewhileFileSending = False
        self.lock = threading.RLock()
        self.size =0
        self.sendedPacket =0




        super(SendFile, self).__init__()



    def run(self):
        while True:
            item = self.q.get()
            if item is None:
                print ("Nincs elem")
            else:

                self.canContinue = False

                item[1] = self.btserver.packet.getNextSequence()
                #print ("Sequence: " + str(item[1]))

                if(item[0] == 7):
                    for i in item:
                        self.client_sock.send(i.to_bytes(1, byteorder='little'))
                else:
                    for fixValues in item[0:4]:
                        self.client_sock.send(fixValues.to_bytes(1, byteorder='little'))
                    # print (" Fájl küldés maradék-----")
                    self.client_sock.send(item[4:].decode('latin1'))

                self.btserver.waitForAck(True, item[1])
                self.btserver.waitForSpecifySeq = item[1]



                while True:
                    if self.canContinue:
                        # print(colored("Megy Tovább", "green"))
                        self.q.task_done()
                        self.size -=1
                        print ("Csomag maradt: " + str(self.size))
                        self.canContinue = False
                        self.sendedPacket +=1
                        break
                    if self.canStopSending:
                        self.canStopSending =False
                        break
                    if self.hasErrorBewhileFileSending:
                        self.hasErrorBewhileFileSending =False
                        for fixValues in item[0:4]:
                            self.client_sock.send(fixValues.to_bytes(1, byteorder='little'))

                        self.btserver.waitForAck(True, item[1])
                        # print (" Fájl küldés maradék-----")
                        self.client_sock.send(item[4:].decode('latin1'))




    def appendToQueue(self,packets):
        with self.lock:
            if(self.size<4):
                self.size = len(packets)
            for key, value in packets.items():
                self.q.put(value)

    def appendToQueueTf(self, tfresult):
        with self.lock:
            self.q.put(tfresult)

    def clearQueueAndStopSending(self):
        with self.lock:
            if(self.q.qsize()>0):
                self.btserver.waitForAck(False, 0)
                self.canStopSending = True
        with self.q.mutex:
            self.q.queue.clear()




    def canContinueSend(self,hasError):
        with self.lock:
            #print(colored("Ack setting", "green"))
            if hasError:
                self.hasErrorBewhileFileSending = True
                self.canContinue = False
            else:
                self.canContinue = True
                self.hasErrorBewhileFileSending = False


            #print(colored("Seq " + str(self.waitForSpecifySeq), "green"))




















