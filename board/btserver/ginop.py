#!/usr/bin/python3
import threading
import time
import bluetooth
import unittest
from enum import Enum
import array
import codecs
import struct
import os

from termcolor import colored
import subprocess
from wifihandler import Finder
import socket




class GinopDevice:
    def __init__(self,client_sock, btserver, commontools):
        self.client_sock = client_sock
        self.btserver = btserver
        self.seq = 0
        self.hardwareVersion = b'\x01\x00'
        self.firmwareVersion =b'\x01\x00'
        self.deviceType =  b'\x07'
        self.c = commontools

        self.isCharging = False

    def setVersionFromPacket(self,packetData,seq):
        self.dateTimeBytes = packetData[4:8]
        self.epochTimeInteger = int.from_bytes(self.dateTimeBytes, byteorder='little')
        self.epochTime = time.ctime(self.epochTimeInteger)
        self.peerType = packetData[8]
        self.osVersion = packetData[9:11]
        self.softVersion = packetData[11:]
        self.seq = seq
        print("Epoch time bytes: ")
        print(*self.dateTimeBytes)
        print("Epoch time integer:")
        print(self.epochTimeInteger)
        print("Datetime from Epoch")
        print(self.epochTime)

        subprocess.run(["date", "--set="+self.epochTime])
        subprocess.run(["timedatectl", "set-timezone","Europe/Budapest"])
        subprocess.run(["hwclock", "--systohc"])

        print("PeerType")
        print(self.peerType)
        print(" Os Version: ")
        print(*self.osVersion)
        print(" Soft Version: ")
        print(*self.softVersion)

        sendAnswerAsVersion = self.getVersion()
        print("Válasz verzió bájtok")
        print(*sendAnswerAsVersion)
        for i in sendAnswerAsVersion:

            self.client_sock.send(i.to_bytes(1, byteorder='little'))

    def ackSetMicrophoneParamters(self,packet,ackPacket):
        self.sample = packet[4]
        self.recordTime = packet[5]
        self.threshold = packet[6]

        self.currentMicSettings = [self.switch_SampleRate(self.sample),self.switch_recodTime(self.recordTime),self.switch_Threshold(self.threshold)]

        print("Ack for Microphone Paramters")
        self.c.sendMessageViaClient("8")
        print(*ackPacket)

        for i in ackPacket:
            self.client_sock.send(i.to_bytes(1, byteorder='little'))



    def ackSoundDataStartRequest(self,ackPacket):
        print("Send Ack for Start Request Sound Command. Values:")
        self.c.sendMessageViaClient("3")
        print(*ackPacket)

        for i in ackPacket:
            self.client_sock.send(i.to_bytes(1, byteorder='little'))

    def speedTest(self):

        self.data ="a"
        self.ettime = time.time()
        self.currentTime =time.time()
        self.speed =0
        while True:
            self.currentTime=time.time()
            if (self.currentTime-self.ettime)   >=1:
                self.ettime = time.time()
                self.currentTime = time.time()

                print ("Sebesség: " + str(self.speed) + " Adat hossza: " + str(len(self.data)))
                self.speed=0
                self.data += "aaaaaaaaaa"
            else:
                self.client_sock.send(self.data)
                self.speed+= len(self.data)

    def ackSoundDataStopRequest(self,ackPacket):
        print("Send Ack for Stop Request Sound Command. Values:")

        self.btserver.s.clearQueueAndStopSending()
        self.c.sendMessageViaClient("5")
        print(*ackPacket)

        for i in ackPacket:
            self.client_sock.send(i.to_bytes(1, byteorder='little'))

    def ackTensorflowStartRequest(self,ackPacket):
        print("Send Ack for Start Request Sound Command. Values:")
        self.c.sendMessageViaClient("6")
        print(*ackPacket)

        for i in ackPacket:
            self.client_sock.send(i.to_bytes(1, byteorder='little'))

    def sendTensorFlowResult(self,packet):
        print ("Tensorflow result packet")
        print (list(packet))
        for i in packet:
            b = i.to_bytes(1, byteorder='little')
            self.client_sock.send(b)

    def ackTensorflowStopRequest(self,ackPacket):
        print("Send Ack for Start Request Sound Command. Values:")
        self.c.sendMessageViaClient("7")
        print(*ackPacket)


    def sendError(self, errorPacket):
        print("Hiba kiküldése Csomag tartalma:")
        print(*errorPacket)


        for i in errorPacket:
            self.client_sock.send(i.to_bytes(1, byteorder='little'))

    def sendDeviceIsReady(self):
        deviceIsReadyPacket = self.btserver.packet.createNewPacketForAck(0x0D)

        for i in deviceIsReadyPacket:
            self.client_sock.send(i.to_bytes(1, byteorder='little'))

    def ackPowerManagementStatus(self,packetBytes,ackPacket):

        for i in ackPacket:
            self.client_sock.send(i.to_bytes(1, byteorder='little'))


        if(packetBytes[4] == 0):
            os.system("shutdown")
        if (packetBytes[4] == 1):
            os.system("reboot")
        if (packetBytes[4] == 2):
            print("Reset")
        if (packetBytes[4] == 3):
            print("Factory Reset")
        if (packetBytes[4] == 4):
            print("Restart Program")


    def getVersion(self):
        print("Verzió válasz csomag")
        newba = []
        newba.append(self.deviceType[0])
        for byteValue in self.hardwareVersion:
            newba.append(byteValue)
        for byteValue in self.firmwareVersion:
            newba.append(byteValue)

        versionPacket =self.btserver.packet.createNewPacket(0x00,newba,True )

        return versionPacket

    def switch_SampleRate(self,argument):
        switcher = {
            0:4000,
            1: 8000,
            2: 16000,
            3: 44500
        }
        print (colored(switcher.get(argument, "Invalid Samplerate"), "yellow"))
        return switcher.get(argument, "Invalid Samplerate")

    def getDeviceStatus(self):
        newba = []


        newba.append(int(self.c.isDeviceReady))
        newba.append(int(self.c.isRecording))
        newba.append(int(self.c.isEvaulating))
        filename = '/sys/class/power_supply/rk-bat/status'

        f = open(filename, "r")  # Open file on read mode
        line = f.readline()
        self.isCharging = "Not" not in line;
        f.close()
        newba.append(int(self.isCharging))
        newba.append(int(self.is_internet_available()))
        deviceStatusPacket = self.btserver.packet.createNewPacket(0x00, newba, True)
        print("DeviceStatus Packet")
        print(*deviceStatusPacket)

        for i in deviceStatusPacket:
            self.client_sock.send(i.to_bytes(1, byteorder='little'))

    def is_internet_available(self):
        try:
            # see if we can resolve the host name -- tells us if there is
            # a DNS listening
            host = socket.gethostbyname("www.google.com")
            # connect to the host -- tells us if the host is actually
            # reachable
            s = socket.create_connection((host, 80), 2)
            s.close()
            return True
        except:
            pass
        return False


    def setWifiParameters(self,packetBytes,ackPacket):
        print(*packetBytes)
        startindex = 4
        ssidWifiLenght = packetBytes[startindex]
        print("SSID")
        ssidIndex = startindex+1;
        ssid = packetBytes[ssidIndex:ssidIndex+ssidWifiLenght]
        print(ssid.decode("utf-8"))
        index = ssidIndex+ssidWifiLenght
        print("Password")
        passwordLenght = packetBytes[index]
        passwordIndex = index+1
        passWord= packetBytes[passwordIndex:passwordIndex+passwordLenght]
        print(passWord.decode("utf-8"))

        #ssid = "pentavox"
        #passkey = "P3entav0x9"





        F = Finder(ssid.decode("utf-8"),passWord.decode("utf-8"))
        #F.run()
        #F.FindFromSearchList()
        #F.Connect()


        for i in ackPacket:
            self.client_sock.send(i.to_bytes(1, byteorder='little'))



    def switch_recodTime(self,argument):
        switcher = {
            0: 2,
            1: 3,
            2: 4,
            3: 5,
        }
        print (colored(switcher.get(argument, "Invalid recordTime"), "yellow"))
        return switcher.get(argument, "Invalid Samplerate")

    def switch_Threshold(self,argument):
        switcher = {
            0: 0.1,
            1: 0.2,
            2: 0.3,
            3: 0.4,
            4: 0.5,
            5: 0.6,
            6: 0.7,
            7: 0.8,
            8: 0.9,
            9: 1.0
        }
        print(colored(switcher.get(argument, "Invalid Threshold"), "yellow"))
        return switcher.get(argument, "Invalid Samplerate")

'''g = GinopDevice()
g.getVersion()'''

