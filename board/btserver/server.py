#!/usr/bin/python3

import threading
import time
import bluetooth
from enum import Enum
from termcolor import colored
import array
import socket
import multiprocessing
import subprocess
# some_file.py
import sys

from datetime import datetime
# insert at 1, 0 is the script path (or '' in REPL)
import pydevd_pycharm
#pydevd_pycharm.settrace('192.168.10.37', port=12345, stdoutToServer=True, stderrToServer=True)
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.append('/root/ginop/btserver/')
from packet import Packet
from ginop import GinopDevice
from sendfile import SendFile
import commontools as c



import logging
import sys




class BtServer(threading.Thread):
    def __init__(self):
        print (colored("BtServerLétrehozva", "magenta"))
        self.log_file = '/media/external/logs/btserver/' + str(datetime.utcnow().strftime('%m_%d_%Y_%I_%M_%S')) + '.log'
        # second file logger
        self.c = c
        self.bts_logger = c.setup_logger('bts_logger', self.log_file)
        self.bts_logger.info("BtServerinicializálás")
        self.lock = threading.RLock()
        self.waitForSendPacketAck = False
        self.waitForSpecifySeq =255
        self.canContinue = False
        self.makeFakeAck = False
        self.ginopDevice = None
        self.client_sock = None
        super(BtServer, self).__init__()

        # Calling destructor
    def __del__(self):
        print("Destructor called")

    def run(self):
        subprocess.call(['sudo', 'hciconfig', 'hci0', 'piscan'])
        self.server_sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
        time.sleep(1)
        c.sendMessageViaClient("21")
        print (colored("Waiting for connection", "green"))
        self.bts_logger.info("Waiting for connection")
        # Set running to True and reset previously recorded frames
        self.server_sock.bind(("", bluetooth.PORT_ANY))
        self.server_sock.listen(1)

        bluetooth.advertise_service(self.server_sock, "helloService",
                                    service_classes=[bluetooth.SERIAL_PORT_CLASS],
                                    profiles=[bluetooth.SERIAL_PORT_PROFILE])

        self.client_sock, self.address = self.server_sock.accept()
        print("Accepted connection from ", self.address)
        self.bts_logger.info("Accepted connection ")
        c.sendMessageViaClient("22")
        self.s = SendFile(self.client_sock, self)
        self.s.start()
        self.ginopDevice = GinopDevice(self.client_sock,self,self.c)
        
        self.packet = Packet(self.ginopDevice,self)

        self.receive_data()


    def waitForAck(self,waitForSendPacketAck,seq):
        with self.lock:

            print(colored("Ack setting", "green"))
            self.waitForSendPacketAck = waitForSendPacketAck
            self.waitForSpecifySeq = seq
            self.makeFakeAck = True

            print(colored("Seq " + str(self.waitForSpecifySeq), "green"))

    

    def getGinop(self):
        return self.ginopDevice


    def receive_data(self):
        try:
            while True:

                data = self.client_sock.recv(1024)
                if not data:
                    break

                print(colored("Data items: " + str(list(data)), "green"))
                #testversion
                #testList = [2, 2, 3, 207, 5, 119, 223, 5, 1, 1, 0, 1, 0]

                #testsound requeststart
                #testList = [3, 2, 0, 117]

                # testsound requeststop
                #testList = [5, 2, 0, 164]

                #packet.setPacket(data[:-2])

                # testsound requeststop
                # testList = [5, 2, 0, 164]

                # testsound requeststop
                testList = [9, 2, 3, 78,1,1,2]

                if(self.waitForSendPacketAck):
                    print(colored("Folytatja", "green"))
                    if(data[0] == 0):
                        self.s.canContinueSend(False)
                    if(data[0]==1):
                        self.s.canContinueSend(True)

                self.packet.setPacket(data)

        except Exception as e:
            self.bts_logger.info("Kapcsolat megszakadt.")
            self.s.clearQueueAndStopSending()

            if (self.client_sock != None):
                self.client_sock.close()
            if (self.server_sock != None):
                self.server_sock.close()

            c.sendMessageViaClient("23")

            print("Kapcsolat megszakadt." )

    def clearSockets(self):
        if (self.client_sock != None):
            self.client_sock.close()
        if (self.server_sock != None):
            self.server_sock.close()

    def SendSoundDataAndWaitForAck(self,packets):
        self.s.appendToQueue(packets)

















