import os
import wifi

class Finder:
    def __init__(self, ssid,password):
        self.ssid = ssid
        self.password = password

    def Search(self):
        try:
            wifilist = []

            cells = wifi.Cell.all('wlan0')

            for cell in cells:
                wifilist.append(cell)
    
            return wifilist
        except Exception as e:
            print(e)

    def FindFromSearchList(self):
        wifilist = self.Search()

        for cell in wifilist:
            if cell.ssid == self.ssid:
                return cell

        return False

    def FindFromSavedList(self):
        cell = wifi.Scheme.find('wlan0', self.ssid)

        if cell:
            return cell

        return False

    def Connect(self):
        cell = self.FindFromSearchList(self.ssid)

        if cell:
            savedcell = self.FindFromSavedList(cell.ssid)

            # Already Saved from Setting
            if savedcell:
                savedcell.activate()
                return cell

            # First time to conenct
            else:
                if cell.encrypted:
                    if password:
                        scheme = self.Add(cell, self.password)

                        try:
                            scheme.activate()

                        # Wrong Password
                        except wifi.exceptions.ConnectionError:
                            self.Delete()
                            return False

                        return cell
                    else:
                        return False
                else:
                    scheme = self.Add(cell)

                    try:
                        scheme.activate()
                    except wifi.exceptions.ConnectionError:
                        self.Delete()
                        return False

                    return cell

        return False

    def Add(self,cell):
        if not cell:
            return False

        scheme = wifi.Scheme.for_cell('wlan0', cell.ssid, cell, self.password)
        scheme.save()
        return scheme

    def Delete(self):
        if not self.ssid:
            return False

        cell = self.FindFromSavedList(self.ssid)

        if cell:
            cell.delete()
            return True

        return False



if __name__ == "__main__":
    # Server_name is a case insensitive string, and/or regex pattern which demonstrates
    # the name of targeted WIFI device or a unique part of it.
    server_name = "example_name"
    password = "your_password"
    interface_name = "your_interface_name" # i. e wlp2s0
    F = Finder(server_name=server_name,
               password=password,
               interface=interface_name)
    F.run()