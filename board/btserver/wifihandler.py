import os
import subprocess


class Finder:
    def __init__(self,server,pwd):
        self.server_name = server
        self.password = pwd
        self.interface_name = "wlan0"
        self.main_dict = {}

        self.set_new_password()


    def set_new_password(self):
        with open('/etc/wpa_supplicant/wpa_supplicant.conf', 'r') as f:
            in_file = f.readlines()
            f.close()

        out_file = []
        hasPsk = False
        for line in in_file:
            if line.startswith("psk"):
                hasPsk = True


        if hasPsk:
            for line in in_file:
                if line.startswith("ssid"):
                    line = 'ssid=' + self.server_name  + '\n'
                if line.startswith("psk"):
                    line = 'psk=' + self.password + '\n'
                out_file.append(line)

            with open('/etc/wpa_supplicant/wpa_supplicant.conf', 'w') as f:
                for line in out_file:
                    f.write(line)
        else:
            setPass = 'sudo wpa_passphrase ' + self.server_name + ' ' + self.password  + ' | sudo tee /etc/wpa_supplicant/wpa_supplicant.conf'
            subprocess.Popen(setPass.split())

        killDhcp = 'dhclient -r'
        restartService = 'systemctl restart wpa_supplicant'

        restartNtwk = "sudo dhclient wlan0"
        subprocess.Popen(killDhcp.split())
        subprocess.Popen(restartService.split())
        subprocess.Popen(restartNtwk.split())




