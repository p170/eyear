# !/usr/bin/python3

import threading
import time
import bluetooth
from commontools import commonTools
from ginop import GinopDevice
from enum import Enum
import array
import codecs
import base64
import sys
import binascii




class PacketError(Enum):
    PacketChecksumError = b'\x00\x01'
    HardwareFailure = b'\x00\x02'
    UnknownCommand = b'\x00\x03'
    DataLenghtSizeError = b'\x00\x04'
    PacketDataTimeout = b'\x00\x05'
    SequenceNumberError = b'\x00\x06'
    BootloaderError = b'\x00\x06'
    AuthenticationError = b'\x00\x08'


class PacketCmd(Enum):
    Ack = 0x00
    ResponseError = 0x01
    Version = 0x02
    RequestSoundData = 0x03
    SoundData = 0x04
    StopSoundData = 0x05
    RequestTensorFlow = 0x06


class Packet:

    def __init__(self, ginopDevice,btserver):
        self.packetBytes = None
        self.g = ginopDevice
        self.cmd = None
        self.seq = 0
        self.data = None
        self.crc = None
        self.btserver = btserver

    def setPacket(self, packetbytes):
        self.packetBytes = packetbytes;
        #print("Set Packet ag")

        if len(self.packetBytes) < 4:
            print("nincs elég argomentum")

            errorPacket = self.createNewPacket(PacketCmd.ResponseError.value, PacketError.DataLenghtSizeError.value,True)
            self.g.sendError(errorPacket)
            return




        self.cmd = self.packetBytes[0]
        self.seq = self.packetBytes[1]
        self.packetLenght = self.packetBytes[2]
        self.crc = self.packetBytes[3]
        if len(self.packetBytes) != 4:
            self.data = self.calcPacketData(self.packetBytes)
        self.checkPacket()

    def checkPacket(self):
        try:
            #print("CheckPacket ág")
            print(self.packetBytes)
            packetCrc = self.packetBytes[3]
            b = c.CRC8_Table(self.packetBytes)

            if (packetCrc.to_bytes(1, byteorder='little') != b):
                #print("Hibas crc")
                print("Hibás crc csomag előállítása")
                print("Várt crc " + str(packetCrc) + " Kiszámolt crc: " + str(b))
                errorPacket = self.createNewPacket(PacketCmd.ResponseError.value,
                                                   PacketError.PacketChecksumError.value, True)
                print("Hibás crc csomag kiküldése")
                self.g.sendError(errorPacket)
                return
            else:
                print("Crc OK")

            receivedPacketLenght = len(self.calcPacketData(self.packetBytes))

            if self.packetLenght != receivedPacketLenght:
                print("Hibás hossz   Küldött:  " + str(self.packetLenght) + "  Kapott:  " + str(receivedPacketLenght))
                print("Hibás csomaghossz csomag előállítása")
                errorPacket = self.createNewPacket(PacketCmd.ResponseError.value,
                                                   PacketError.DataLenghtSizeError.value, True)
                print("Hibás csomaghossz csomag kiküldése")
                self.g.sendError(errorPacket)
                return
            else:
                print("Lenght OK")

            #print("Setstate ág")
            self.setCommandStatement(self.cmd)
            #print("Checkpacket ág visszatér")
 
        except Exception as e:

            print("HIBA:" +e)


    def createNewPacketForAck(self,cmd):
        newba = bytearray()
        #print("Add cmd: " + str(cmd))
        newba.append(cmd)
        #print("Add seq: " + str(self.seq))
        newba.append(self.seq)
        #print("Add packetlenght: 0")
        newba.append(0)
        #print("Add empty crc")
        newba.append(0)
        newba[3] = c.CRC8_Table(newba)[0]
        #print("Modify crc: " + str(newba[3]))

        #print("Create new packet ág visszatér")
        return bytearray(newba)

    def createNewPacket(self, cmd, data, isAnswer):
        #print("Create new packet ág")
        newba = bytearray()
        
        if not isAnswer:
            #print("Ez egy új csomag a seq növelése eggyel")
            self.seq +=1
            if self.seq > 255:
                self.seq =0

        #print("Add cmd: " + str(cmd))
        newba.append(cmd)
        #print("Add seq: " + str(self.seq))
        newba.append(self.seq)
        #print("Add packetlenght: " + str(len(data)))

        if(isinstance(data, int)):
            data =data.to_bytes(1, byteorder='little')

        newba.append(len(data))
        # print("Add empty crc")
        newba.append(0)

        #print("Foreach data : " + str((type(data))))

        for byteValue in data:
            try:

                newba.append(byteValue)
            except Exception as e:
                print (e)
                #print ("Bytevalue: " + str(byteValue) )




        newba[3] =c.CRC8_Table(newba)[0]
        #print("Modify crc: " + str(newba[3]))

        #print("Create new packet ág visszatér")
        return bytearray(newba)



    # Yield successive n-sized
    # chunks from l.
    def divide_chunks(self,l, n):

        # looping till length l
        for i in range(0, len(l), n):
            yield l[i:i + n]

            # How many elements each

    # list should have



    def makePacketsFromFile(self,filename):

        print("Make Packet ag")
        separetedPacketBytes = {}


        f = open(filename, 'rb')
        chunk = f.read()
        crc32v = self.CRC32_from_buffer(chunk)
        n =180


        x = list(self.divide_chunks(chunk, n))
        index = self.seq
        encoded = base64.b64encode(len(x).to_bytes(2, byteorder="little"))
        separetedPacketBytes.update({index: self.createNewPacket(4, encoded , False)})

        for i in x:
            index +=1
            encoded = base64.b64encode(i)
            separetedPacketBytes.update({index:self.createNewPacket(4,encoded,False)})

        f.close()

        # Make crc32 value to last packet
        separetedPacketBytes.update({index: self.createNewPacket(4, crc32v, False)})

        self.btserver.SendSoundDataAndWaitForAck(separetedPacketBytes)

    def CRC32_from_buffer(self,buf):
        buf = (binascii.crc32(buf) & 0xFFFFFFFF)
        print("Crc32 calcualted result: " + str("%08X" % buf))
        result = bytearray.fromhex( "%08X" % buf)
        return result

    def getNextSequence(self):
        tempseq = self.seq
        tempseq +=1
        if tempseq >255:
            tempseq =0
        return tempseq

    def calcPacketData(self, packetbytes):
        packetOfData = bytearray()
        for val in packetbytes[4:]:
            b = val.to_bytes(1, byteorder='little')
            packetOfData += bytes(b)
        return packetOfData

    def getPacketData(self):
        return self.data

    def setCommandStatement(self, cmd):
        try:
            print(cmd)

            def ack():
                print("Ack parancs érkezett")
                return ("acked")

            def error():
                print("Error parancs érkezett")
                return ("Get Error")

            def version():
                print("Verzió csomag érkezett")
                self.g.setVersionFromPacket(self.packetBytes,self.seq)
                return ("Version set")

            def requestSound():
                print("Hangkérés parancs érkezett")
                ackPacket = self.createNewPacketForAck(PacketCmd.Ack.value)
                self.g.ackSoundDataStartRequest(ackPacket)
                return "requestsounddata"

            def soundData():
                return "send sound data"

            def stopSound():
                print("Hangkérés leállítás parancs érkezett")
                ackPacket = self.createNewPacketForAck(PacketCmd.Ack.value)
                self.g.ackSoundDataStopRequest(ackPacket)
                return "stop sound data"

            def requestTensorflow():
                print("TEnsorflow kérés parancs érkezett")
                ackPacket = self.createNewPacketForAck(PacketCmd.Ack.value)
                self.g.ackTensorflowStartRequest(ackPacket)
                return "requestTEnsorflowdata"

            def TensorFlowResult():
                return "send sound data"

            def stopTensorflow():
                print("TEnsorflow leállítása parancs érkezett")
                ackPacket = self.createNewPacketForAck(PacketCmd.Ack.value)
                self.g.ackTensorflowStopRequest(ackPacket)
                return "stop TEnsorflow data"

            def setMicrophoneParameters():
                print("Hangkérés parancs érkezett")
                ackPacket = self.createNewPacketForAck(PacketCmd.Ack.value)
                self.g.ackSetMicrophoneParamters(self.packetBytes,ackPacket)
                return "send sound data"

            def firmwareUpgrade():
                print("FirmwareUpgradeRqeuest")

            def deviceStatus():
                print("GetDeviceStatus")
                self.g.getDeviceStatus()

            def wifiParameters():
                print("Wifi parameters command receive")
                ackPacket = self.createNewPacketForAck(PacketCmd.Ack.value)
                self.g.setWifiParameters(self.packetBytes, ackPacket)

            def powerManagement():
                print("Power management command receive")
                ackPacket = self.createNewPacketForAck(PacketCmd.Ack.value)
                self.g.ackPowerManagementStatus(self.packetBytes, ackPacket)

            def default():
                print("Nincs ilyen parancskód. Válasz küldése")
                errorPacket = self.createNewPacket(PacketCmd.ResponseError.value,
                                                   PacketError.UnknownCommand.value,True)
                self.g.sendError(errorPacket)
                return "Incorrect command"

            switcher = {
                0: ack,
                1: error,
                2: version,
                3: requestSound,
                4: soundData,
                5: stopSound,
                6: requestTensorflow,
                7: TensorFlowResult,
                8: stopTensorflow,
                9: setMicrophoneParameters,
                10: firmwareUpgrade,
                11: deviceStatus,
                12: wifiParameters,
                15: powerManagement
            }

            def switch(cmd):
                return switcher.get(cmd, default)()

            p =switch(cmd)


        except Exception as e:
            errorPacket = self.createNewPacket(PacketCmd.ResponseError.value, PacketError.DataLenghtSizeError.value,True)
            self.g.sendError(errorPacket)


c = commonTools()

'''testList = [2, 2, 3, 207, 5, 119, 223, 5, 1, 1, 0, 1, 0]

g = GinopDevice()
p = Packet(g)
p.setPacket(testList)

t = p.getPacketData()
print("Exiting Main Thread")'''

