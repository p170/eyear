set prompt "#"

send_user "\nRestarting bluetooth service.\r"
spawn sudo service bluetooth restart

send_user "\nConnecting to MS-10DMKII.\r"
spawn bluetoothctl
expect -re $prompt
expect "Agent registered"
send discoverable on

send "quit\r"
expect eof