#!/usr/bin/python

from periphery import GPIO
import time
import atexit
import threading


class LedThread (threading.Thread):

    def __init__(self):

        self.blueLedState = 0
        self.redLedState = 0
        self.lock = threading.RLock()
        self.pinNumBlue = 38;
        self.BlUELED = GPIO(self.pinNumBlue, "out")
        self.pinNumRed = 28;
        self.REDLED = GPIO(self.pinNumRed, "out")
        super(LedThread, self).__init__()

    def set_recordingState(self, redLed):  # you can use a proper setter if you want
        with self.lock:
            self.redLedState = redLed

    def set_connectionState(self, blueLed):  # you can use a proper setter if you want
        with self.lock:

            self.blueLedState = blueLed

    def run(self):
        while True:
            with self.lock:
                if self.blueLedState ==0:
                    self.BlUELED.write(False)

                if self.blueLedState ==1:
                    self.BlUELED.write(True)
                    time.sleep(1)
                    self.BlUELED.write(False)
                    time.sleep(1)

                if self.blueLedState ==2:
                    self.BlUELED.write(True)

                if self.redLedState == 0:
                    self.REDLED.write(False)

                if self.redLedState == 1:
                    self.REDLED.write(True)
                    time.sleep(1)
                    self.REDLED.write(False)
                    time.sleep(1)

                if self.redLedState ==2:
                    self.REDLED.write(True)




            time.sleep(0.1)  # let it breathe

    def setLedParametersToDefaultAfterProgramShutdowned(self):
        with self.lock:
            self.redLedState = 0
            self.blueLedState = 0
            self.BlUELED.write(False)
            self.REDLED.write(False)




















