#!/usr/bin/python

from periphery import GPIO
import time
import atexit
import threading
from rcpcoreThread import ThreadedServer
import pydevd_pycharm
pydevd_pycharm.settrace('192.168.0.104', port=12345, stdoutToServer=True, stderrToServer=True)

class LedThread (threading.Thread):

    def __init__(self):
        self.rcp = None
        self.blueLedState = 0
        self.redLedState = 0
        self.lock = threading.RLock()
        self.pinNumBlue = 38;
        self.BlUELED = GPIO(self.pinNumBlue, "out")
        self.pinNumBlueButton = 32;
        self.pinNumRedButton = 30;
        self.BlueButton = GPIO(self.pinNumBlueButton, "in")
        self.RedButton = GPIO(self.pinNumRedButton, "in")
        self.pinNumRed = 28;
        self.REDLED = GPIO(self.pinNumRed, "out")
        self.isRunning = False
        super(LedThread, self).__init__()

    def set_recordingState(self, redLed):  # you can use a proper setter if you want
        with self.lock:
            self.redLedState = redLed

    def set_connectionState(self, blueLed):  # you can use a proper setter if you want
        with self.lock:

            self.blueLedState = blueLed

    def run(self):
        try:
            while True:
                with self.lock:
                    if  self.isRunning == self.BlueButton.read() == False:
                        self.setRunningState(True)
                        print("Bejött ebbe az ágba")
                        self.rcp = ThreadedServer()
                        self.rcp.start()

                    if self.isRunning == True and self.RedButton.read() == False:
                        self.setRunningState(False)
                        print("Bejött ebbe az ágba")
                        self.rcp.killAllthreads()
                        self.rcp = None
        except Exception as e:
            print(e)




    def setRunningState(self,isRunning):
        with self.lock:
            self.isRunning = isRunning;


l = LedThread()
l.start()

















