import pyaudio
import wave
import subprocess
import os
import time
import threading
import numpy as np
from python_speech_features import mfcc
from python_speech_features import delta
from python_speech_features import logfbank
import scipy.io.wavfile as wav
import glob
from pathlib import Path
from termcolor import colored
from datetime import datetime
import logging
import sys
import traceback
import commonTools as c
from zipfile import ZipFile





#Save file to filename location as a wavefront file.
def saveMfcc(wavFile):
    try:


        #print("extracting features {0}", wavFile)
        resultsDirectory = '/media/external/MFCCresults'
        if not os.path.exists(resultsDirectory):
            os.makedirs(resultsDirectory)

        zippedFileDirecotry = '/media/external/zips'
        if not os.path.exists(zippedFileDirecotry):
            os.makedirs(zippedFileDirecotry)
        (rate, sig) = wav.read(wavFile)
        mfcc_feat = mfcc(sig, rate, winstep=0.085, nfilt=60, nfft=4096, numcep=41)
        print(mfcc_feat.shape)

        fbank_feat = logfbank(sig, rate, winstep=0.165, nfilt=41, nfft=4096)
        #print(fbank_feat.shape)


        # Get FileName without Extension From Path
        p = Path(wavFile)

        # create a file to save our results in
        #outputFile = resultsDirectory + "/" + p.stem + "_fbank_feat_4096.npy"
        #print ("output fbank: " + outputFile)
        #file = open(outputFile, 'wb')  # make file/over write existing file
        #tempfile = file

        #array_lst = np.array(fbank_feat, dtype=float)
        #np.save(file, array_lst)
        # numpy.savetxt(file, fbank_feat, delimiter=",") #save MFCCs as .csv

        # create a file to save our results in
        outputFile = resultsDirectory + "/" + p.stem + "_mfcc_feat_4096.npy"
        #print ("output fbank: " + outputFile)
        file = open(outputFile, 'wb')  # make file/over write existing file
        np.save(file, mfcc_feat)
        #print(colored("Mfcc elküldve", "blue"))

        zippedFileName = str(zippedFileDirecotry + "/" + p.stem + ".zip")

        # Create a ZipFile Object
        with ZipFile(zippedFileName, 'w') as zipObj2:
            # Add multiple files to the zip
            zipObj2.write(wavFile)
            zipObj2.write(file.name)

        c.sendMessageViaClient("4" + zippedFileName)


    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
        mfccsaver_logger.error("Hiba történt: " + ''.join('!! ' + line for line in lines))

        print (colored("Hiba történt az mfcc mentés szálban. Részletek a logban"  + log_file,"red"))











log_file = '/media/external/logs/mfccsaver/' + str(datetime.utcnow().strftime('%m_%d_%Y_%I_%M_%S')) + '.log'
# second file logger
mfccsaver_logger = c.setup_logger('mfccsaver_logger', log_file)
mfccsaver_logger.debug("Mfcc saver inicializálás")

print(colored("Mfcc saver inicializálás", "magenta"))




