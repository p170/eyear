import pyaudio
import math
import struct
import wave
import time
import os
from statistics import mean
from datetime import datetime
import _thread
import logging
import sys
import traceback
from termcolor import colored
import sys
from timeloop import Timeloop
from datetime import timedelta
import threading
from threading import Thread


#Osztályok
sys.path.insert(1, '/root/ginop/listener/')
import commonTools as c

from soundrecorder import SoundRecorder





t1 = None




class Recorder(threading.Thread):


    def rms(self,frame):
        count = len(frame) / self.swidth
        format = "%dh" % (count)
        shorts = struct.unpack(format, frame)

        sum_squares = 0.0
        for sample in shorts:
            n = sample * self.SHORT_NORMALIZE
            sum_squares += n * n
            rms = math.pow(sum_squares / count, 0.5)

            return rms * 1000

    def __init__(self):
        self.p = pyaudio.PyAudio()
        self.Threshold = 0.3

        self.SHORT_NORMALIZE = (1.0 / 32768.0)
        self.chunk = 1024
        self.FORMAT = pyaudio.paInt16
        self.CHANNELS = 2
        self.RATE = 8000
        self.swidth = 2
        self.chunklist = [0.0]
        self.TIMEOUT_LENGTH = 5
        self.stream = self.p.open(format=self.FORMAT,
                                  channels=self.CHANNELS,
                                  rate=self.RATE,
                                  input=True,
                                  output=True,
                                  frames_per_buffer=self.chunk)
        self.soundRec = SoundRecorder()

        self.lock = threading.RLock()
        super(Recorder, self).__init__()

    def run(self):
        try:
            print('Listening beginning')
            c.sendMessageViaClient("11")
            while True:
                input = self.stream.read(self.chunk)
                rms_val = self.rms(input)
                self.chunklist.append(rms_val)
                avarage = mean(self.chunklist)
                # print(test)
                # print(mean(chunklist))
                if avarage + self.Threshold < rms_val:
                    self.record()
        except Exception as e:
            c.sendMessageViaClient("10")
            exc_type, exc_value, exc_traceback = sys.exc_info()
            lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
            listener_logger.info("Hiba történt: " + (''.join('!! ' + line for line in lines)))
            print(colored("Hiba történt. Részletek a logban:" + log_file, "red"))

    def setNewMicrophoneParameters(self,micSettings):
        with self.lock:
            self.soundRec = SoundRecorder(micSettings[0])
            self.TIMEOUT_LENGTH = micSettings[1]
            self.Threshold = micSettings[2]

    def record(self):
        try:
            print('Noise detected, recording beginning')
            rec = []
            rectmp = []
            current = time.time()
            end = time.time() + self.TIMEOUT_LENGTH
            currentDate = datetime.now()
            blockSound = datetime.now()


            i = 0
            self.soundRec.recFor5second()
            c.sendMessageViaClient("12")
            while current <= end:

                data = self.stream.read(self.chunk)
                if self.rms(data) >= self.Threshold:
                    end = time.time() + self.TIMEOUT_LENGTH

                delta = currentDate - blockSound
                if delta.total_seconds() >= 5:
                    # Place where i will send data to process
                    blockSound = datetime.now()


                    if blockSound != end:
                        self.soundRec.recFor5second()

                currentDate = datetime.now()
                current = time.time()

            c.sendMessageViaClient("11")
            print (colored("Return to listening.","green"))
            
        except Exception as e:
            c.sendMessageViaClient("10")
            exc_type, exc_value, exc_traceback = sys.exc_info()
            lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
            listener_logger.info("Hiba történt: " + (''.join('!! ' + line for line in lines)))
            print ( colored("Hiba történt. Részletek a logban:" + log_file,"red"))

    # Python program to get average of a list
    def Average(lst):
        return sum(lst) / len(lst)


    def setMicrophoneParameters(self, rate):
        self.soundRec.RATE = rate


##--------------Main-------------
log_file = '/media/external/logs/listener/' + str(datetime.utcnow().strftime('%m_%d_%Y_%I_%M_%S')) + '.log'
# second file logger
listener_logger = c.setup_logger('listener_logger', log_file)
listener_logger.debug("Listener inicializálás")
print(colored("Listener inicializálás", "magenta"))

tl = Timeloop()

@tl.job(interval=timedelta(seconds=60))
def saveCapacity():
    filename = '/sys/class/power_supply/rk-bat/uevent'

    f = open(filename, "r")# Open file on read mode
    lines = f.read().split("\n")  # Create a list containing all lines
    listener_logger.debug("Battery Capacity: " + '\n'.join(lines))
    f.close()



tl.start(block=False)














