import pyaudio
import wave
import subprocess
import os
import time
import threading
from termcolor import colored
import commonTools as c
import mfccsaver
import logging
import sys
import socket
from datetime import datetime
import traceback
from pydub import AudioSegment


class SoundRecorder():
    #Defines sound properties like frequency and channels
    def __init__(self, chunk=1024, channels=1, rate=8000):
        self.CHUNK = chunk
        self.FORMAT = pyaudio.paInt16
        self.CHANNELS = channels
        self.RATE = rate
        self._running = False
        self._frames = []
        self.withOutSaving = False

    def recFor5second(self):
        if not self._running:
            threading._start_new_thread(self.start, ())


    #Start recording sound
    def start(self):
        threading._start_new_thread(self.__recording, ())
        #a stopnak ennyivel több kell mint 5 masodperc
        time.sleep(5.2)
        self.stop()
        self.save()


    def __recording(self):
        #Set running to True and reset previously recorded frames
        self._running = True
        self._frames = []
        #Create pyaudio instance
        p = pyaudio.PyAudio()
        #Open stream
        stream = p.open(format=self.FORMAT,
                        channels=self.CHANNELS,
                        rate=self.RATE,
                        input=True,
                        frames_per_buffer=self.CHUNK)
        # To stop the streaming, new thread has to set self._running to false
        # append frames array while recording
        while(self._running):
            data = stream.read(self.CHUNK)
            self._frames.append(data)

        # Interrupted, stop stream and close it. Terinate pyaudio process.
        stream.stop_stream()
        stream.close()
        p.terminate()






    # Sets boolean to false. New thread needed.
    def stop(self):
        self._running = False

    def stopWithoutSaving(self):
        self.withOutSaving = True

    #Save file to filename location as a wavefront file.
    def save(self):
        try:
            if not self.withOutSaving:
                self.withOutSaving = False
                f_name_directory = '/media/external/wavs'
                n_files = len(os.listdir(f_name_directory))

                filename = os.path.join(f_name_directory, '{}.wav'.format(n_files))
                #print("Saving")
                p = pyaudio.PyAudio()

                wf = wave.open(filename, 'wb')
                wf.setnchannels(self.CHANNELS)
                wf.setsampwidth(p.get_sample_size(self.FORMAT))
                wf.setframerate(self.RATE)
                wf.writeframes(b''.join(self._frames))
                wf.close()
                newAudio = AudioSegment.from_wav(filename)
                newAudio = newAudio[0:5000]
                newAudio.export(filename, format="wav")
                #print("Saved: " + filename)
                mfccsaver.saveMfcc(filename)


        except Exception as e:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
            soundrecorder_logger.error("Hiba történt: " + ''.join(line for line in lines))
            print (colored("Hiba történt az wav mentés szálban. Részletek a logban"  + log_file,"red"))




log_file = '/media/external/logs/soundrecorder/' + str(
    datetime.utcnow().strftime('%m_%d_%Y_%I_%M_%S')) + '.log'
# second file logger
soundrecorder_logger = c.setup_logger('soundrecorder_logger', log_file)


soundrecorder_logger.debug("SoundRecorder saver inicializálás")
print(colored("SoundRecorder saver inicializálás", "magenta"))


'''if __name__ == "__main__":
    rec = Recorder()
    print("Start recording")
    rec.start()
    time.sleep(5)
    print("Stop recording")
    rec.stop()
    print("Saving")
    rec.save("test.wav")
    print("Converting wav to mp3")
    Recorder.wavTomp3("test.wav")
    print("deleting wav")
    Recorder.delete("test.wav")'''