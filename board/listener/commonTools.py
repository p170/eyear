
import math
import struct
import wave
import time
import os
import array
import numpy as np
import array
import codecs
import threading
import socket
import logging

def setup_logger(name, log_file, level=logging.DEBUG):
    formatter = '%(asctime)s.%(msecs)03d %(levelname)s %(module)s - %(funcName)s: %(message)s'

    handler = logging.FileHandler(log_file)
    handler.setFormatter(logging.Formatter(formatter))

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)

    return logger

def sendMessageViaClient(message):
    threading._start_new_thread(__client, (message,))


def __client(message):
    try:
        host = socket.gethostname()  # get local machine name
        port = 12345  # Make sure it's within the > 1024 $$ <65535 range

        s = socket.socket()
        s.connect((host, port))

        while message != 'q':
            s.send(message.encode('utf-8'))
            data = s.recv(1024).decode('utf-8')
            print('Received from server: ' + data)
            message = input('==> ')
        s.close()
    except Exception as e:
        print(e)


