import os
import re, uuid
import subprocess

macaddressOfBlueTooth =':'.join(re.findall('..', '%012x' % uuid.getnode())).encode()

artikName = "Artik_" + macaddressOfBlueTooth.decode('utf8')[-5:]
print(artikName)


subprocess.call(['sudo', 'hciconfig', 'hci0', 'name', artikName])

saveWavDirecotry = '/media/external/wavs'
if not os.path.exists(saveWavDirecotry):
    os.makedirs(saveWavDirecotry)
    print("Wav mentési helye létrehozva.")
else:
    print("Wav mentési helye létezik.")


saveResultDirecotry = '/media/external/MFCCresults'
if not os.path.exists(saveResultDirecotry):
    os.makedirs(saveResultDirecotry)
    print("Mfcc eredmények mentési helye létrehozva.")
else:
    print("Mfcc eredmények mentési helye létezik.")

saveLogDirecotry = '/media/external/logs'
if not os.path.exists(saveLogDirecotry):
    os.makedirs(saveLogDirecotry)
    print("Log könyvtár létrehozva.")
else:
    print("Log könyvtár létezik.")

saveLogListenerDirecotry = '/media/external/logs/listener'
if not os.path.exists(saveLogListenerDirecotry):
    os.makedirs(saveLogListenerDirecotry)
    print("Listener log köynvtár létrehozva.")
else:
    print("Listener log köynvtár létezik.")

saveLogRcpDirecotry = '/media/external/logs/rcp'
if not os.path.exists(saveLogRcpDirecotry):
    os.makedirs(saveLogRcpDirecotry)
    print("Rcp log köynvtár létrehozva.")
else:
    print("Rcp log köynvtár létezik.")

saveLogBluetoothDirecotry = '/media/external/logs/btserver'
if not os.path.exists(saveLogBluetoothDirecotry):
    os.makedirs(saveLogBluetoothDirecotry)
    print("Bluetooth log mentési helye létrehozva.")
else:
    print("Bluetoothlog mentési helye létezik.")

saveLogSoundRecorderDirecotry = '/media/external/logs/soundrecorder'
if not os.path.exists(saveLogSoundRecorderDirecotry):
    os.makedirs(saveLogSoundRecorderDirecotry)
    print("Soundrecorder log mentési helye létrehozva.")
else:
    print("Soundrecorder log mentési helye létezik.")


saveLogMfccDirecotry = '/media/external/logs/mfccsaver'
if not os.path.exists(saveLogMfccDirecotry):
    os.makedirs(saveLogMfccDirecotry)
    print("MfccSaver log mentési helye létrehozva.")
else:
    print("MfccSaver mentési helye létezik.")

saveLogCapacityDirecotry = '/media/external/logs/capacity'
if not os.path.exists(saveLogCapacityDirecotry):
    os.makedirs(saveLogCapacityDirecotry)
    print("Capacity log mentési helye létrehozva.")
else:
    print("Capacity log mentési helye létezik.")
